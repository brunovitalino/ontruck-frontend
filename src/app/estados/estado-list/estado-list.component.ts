import { Component, OnInit } from '@angular/core';
import { Estado } from '../../shared/model/estado';
import { EstadosService } from '../estados.service';

@Component({
  selector: 'app-estado-list',
  templateUrl: './estado-list.component.html',
  styleUrls: ['./estado-list.component.css'],
  preserveWhitespaces: true
})
export class EstadoListComponent implements OnInit {

  pageNumber: number;
  pageSize: number;
  estados: Estado[];
  colunas = [];
  linhas = [];

  constructor(
    private estadosService: EstadosService
  ) { }

  ngOnInit(): void {
    this.loadPageable();
    this.loadColunas();
    this.loadLinhas();
  }

  loadPageable() {
    this.estadosService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.estados = resp.content as Estado[];
    });
  }

  loadColunas() {
    this.colunas = [
      { header: 'Nome' }
    ];
  }

  loadLinhas() {
    this.estadosService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.estados = resp.content as Estado[];
      this.setLinhas(this.estados);
    });
  }

  setLinhas(estados: Estado[]) {
    estados.forEach(e => {
      this.linhas.push([
        { field: e.id },
        { field: e.nome }
      ]);        
    });
  }

  deleteLinha(id) {
    this.estadosService.delete(id).subscribe(res => {
      this.linhas = this.linhas.filter(e => e[0]['field'] != id);
    }, err => {
      if (err.error && err.error.exception) {
        console.log('ERRO', err.error.exception, err.error.cause);
      } else {
        console.log('ERRO', err);
      }
    });
  }

}
