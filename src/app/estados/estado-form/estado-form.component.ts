import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Estado } from 'src/app/shared/model/estado';
import { EstadosService } from '../estados.service';

@Component({
  selector: 'app-estado-form',
  templateUrl: './estado-form.component.html',
  styleUrls: ['./estado-form.component.css']
})
export class EstadoFormComponent implements OnInit {

  estadoId: number;
  estado: Estado;
  linhas: any[] = [];
  teste: string;

  constructor(
    private estadosService: EstadosService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadEstadoId();
    this.loadLinhas();
  }
  
  loadEstadoId() {
    this.estadoId = this.activatedRoute.snapshot.params.id;
  }
  
  isNewForm() {
    return !this.estadoId;
  }

  loadLinhas() {
    if (this.isNewForm()) {
      this.estado = { id: null, nome: "" };
      this.setLinhas(this.estado);
    } else {
      this.estadosService.findOneById(this.estadoId).subscribe(resp => {
        this.linhas = [];
        const estado = resp || null;
        if (estado) {
          this.setLinhas(estado);
        }
      });
    }
  }

  setLinhas(estado: Estado) {
    const keys = Object.keys(estado);
    for(let i = 1; i < keys.length; i+=2) {
      this.linhas.push(keys.slice(i, i+2));
    }
  }

  save() {
    this.estadosService.save(this.estado).subscribe(res => {
      this.router.navigate( [`/estados`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

  update() {
    this.estadosService.update(this.estadoId, this.estado).subscribe(res => {
      this.router.navigate( [`/estados`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

}
