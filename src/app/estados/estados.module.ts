import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrudsModule } from '../shared/cruds/cruds.module';
import { EstadoListComponent } from './estado-list/estado-list.component';
import { EstadoFormComponent } from './estado-form/estado-form.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    EstadoListComponent,
    EstadoFormComponent
  ],
  imports: [
    CommonModule,
    CrudsModule,
    FormsModule
  ]
})
export class EstadosModule { }
