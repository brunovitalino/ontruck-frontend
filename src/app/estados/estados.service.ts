import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { PageResponse } from '../shared/model/page-response';
import { Estado } from '../shared/model/estado';
import { environment } from '../../environments/environment';

const API = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class EstadosService {

  constructor(
    private http: HttpClient
  ) { }
  
  public findAll() {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json'
    });
    return this.http.get<PageResponse>(`${API}/estados`, {headers});
  }
  
  public findOneById(id: number) {
    return this.http.get<Estado>(`${API}/estados/${id}`);
  }
  
  public save(estado: Estado) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.post<Estado>(`${API}/estados`, estado, {headers});
  }
  
  public update(id: number, estado: Estado) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.put<Estado>(`${API}/estados/${id}`, estado, {headers});
  }
  
  public delete(id: number) {
    return this.http.delete<Estado>(`${API}/estados/${id}`);
  }

}
