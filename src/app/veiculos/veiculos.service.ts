import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { PageResponse } from '../shared/model/page-response';
import { Veiculo } from '../shared/model/veiculo';
import { environment } from '../../environments/environment';

const API = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class VeiculosService {

  constructor(
    private http: HttpClient
  ) { }
  
  public findAll() {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json'
    });
    return this.http.get<PageResponse>(`${API}/veiculos`, {headers});
  }
  
  public findOneById(id: number) {
    return this.http.get<Veiculo>(`${API}/veiculos/${id}`);
  }
  
  public save(veiculo: Veiculo) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.post<Veiculo>(`${API}/veiculos`, veiculo, {headers});
  }
  
  public update(id: number, veiculo: Veiculo) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.put<Veiculo>(`${API}/veiculos/${id}`, veiculo, {headers});
  }
  
  public delete(id: number) {
    return this.http.delete<Veiculo>(`${API}/veiculos/${id}`);
  }

}
