import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CoresService } from 'src/app/cores/cores.service';
import { ModelosService } from 'src/app/modelos/modelos.service';
import { Cor } from 'src/app/shared/model/cor';
import { Modelo } from 'src/app/shared/model/modelo';
import { Veiculo } from 'src/app/shared/model/veiculo';
import { VeiculosService } from '../veiculos.service';

@Component({
  selector: 'app-veiculo-form',
  templateUrl: './veiculo-form.component.html',
  styleUrls: ['./veiculo-form.component.css']
})
export class VeiculoFormComponent implements OnInit {

  veiculoId: number;
  veiculo: Veiculo;
  linhas: any[] = [];
  cores: Cor[];
  modelos: Modelo[];

  constructor(
    private veiculosService: VeiculosService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private coresService: CoresService,
    private modelosService: ModelosService
  ) { }

  ngOnInit(): void {
    this.loadCores();
    this.loadModelos();
    this.loadVeiculoId();
    this.loadLinhas();
  }
  
  loadVeiculoId() {
    this.veiculoId = this.activatedRoute.snapshot.params.id;
  }
  
  isNewForm() {
    return !this.veiculoId;
  }

  loadLinhas() {
    if (this.isNewForm()) {
      this.veiculo = { id: null, placa: "", cor: { id: null, nome: "" },
        modelo: { id: null, nome: "", marca: null }, ano: null, kmTotal: null
      };
    } else {
      this.veiculosService.findOneById(this.veiculoId).subscribe(resp => {
        this.linhas = [];
        this.veiculo = resp || null;
      });
    }
  }

  loadCores() {
    this.coresService.findAll().subscribe(resp => {
      this.cores = resp.content as Cor[] || null;
    });
  }

  loadModelos() {
    this.modelosService.findAll().subscribe(resp => {
      this.modelos = resp.content as Modelo[] || null;
    });
  }

  onSelectCor(valor: number) {
    if (valor == 0) {
      this.veiculo.cor = null;
      return;
    }
    this.veiculo.cor = this.cores.find(e => e.id == valor);
  }

  onSelectModelo(valor: number) {
    if (valor == 0) {
      this.veiculo.modelo = null;
      return;
    }
    this.veiculo.modelo = this.modelos.find(e => e.id == valor);
  }

  save() {
    this.veiculosService.save(this.veiculo).subscribe(res => {
      this.router.navigate( [`/veiculos`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

  update() {
    this.veiculosService.update(this.veiculoId, this.veiculo).subscribe(res => {
      this.router.navigate( [`/veiculos`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

}
