import { Component, OnInit } from '@angular/core';

import { Veiculo } from 'src/app/shared/model/veiculo';
import { VeiculosService } from '../veiculos.service';

@Component({
  selector: 'app-veiculo-list',
  templateUrl: './veiculo-list.component.html',
  styleUrls: ['./veiculo-list.component.css']
})
export class VeiculoListComponent implements OnInit {

  pageNumber: number;
  pageSize: number;
  veiculos: Veiculo[];
  colunas = [];
  linhas = [];

  constructor(
    private veiculosService: VeiculosService
  ) { }

  ngOnInit(): void {
    this.loadPageable();
    this.loadColunas();
    this.loadLinhas();
  }

  loadPageable() {
    this.veiculosService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.veiculos = resp.content as Veiculo[];
    });
  }

  loadColunas() {
    this.colunas = [
      { header: 'Placa' },
      { header: 'Cor' },
      { header: 'Modelo' },
      { header: 'Ano' },
      { header: 'kmTotal' }
    ];
  }

  loadLinhas() {
    this.veiculosService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.veiculos = resp.content as Veiculo[];
      this.setLinhas(this.veiculos);
    });
  }

  setLinhas(veiculos: Veiculo[]) {
    veiculos.forEach(e => {
      this.linhas.push([
        { field: e.id },
        { field: e.placa },
        { field: e.cor.nome },
        { field: e.modelo.nome },
        { field: e.ano },
        { field: e.kmTotal }
      ]);
    });
  }

  deleteLinha(id) {
    this.veiculosService.delete(id).subscribe(res => {
      this.linhas = this.linhas.filter(e => e[0]['field'] != id);
    }, err => {
      if (err.error && err.error.exception) {
        console.log('ERRO', err.error.exception, err.error.cause);
      } else {
        console.log('ERRO', err);
      }
    });
  }

}
