import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from './errors/not-found/not-found.component';
import { EstadoListComponent } from './estados/estado-list/estado-list.component';
import { EstadoFormComponent } from './estados/estado-form/estado-form.component';
import { CidadeListComponent } from './cidades/cidade-list/cidade-list.component';
import { CidadeFormComponent } from './cidades/cidade-form/cidade-form.component';
import { CorListComponent } from './cores/cor-list/cor-list.component';
import { CorFormComponent } from './cores/cor-form/cor-form.component';
import { MarcaListComponent } from './marcas/marca-list/marca-list.component';
import { MarcaFormComponent } from './marcas/marca-form/marca-form.component';
import { ModeloListComponent } from './modelos/modelo-list/modelo-list.component';
import { ModeloFormComponent } from './modelos/modelo-form/modelo-form.component';
import { VeiculoListComponent } from './veiculos/veiculo-list/veiculo-list.component';
import { VeiculoFormComponent } from './veiculos/veiculo-form/veiculo-form.component';
import { MotoristaListComponent } from './motoristas/motorista-list/motorista-list.component';
import { MotoristaFormComponent } from './motoristas/motorista-form/motorista-form.component';
import { ViagemListComponent } from './viagens/viagem-list/viagem-list.component';
import { ViagemFormComponent } from './viagens/viagem-form/viagem-form.component';
import { MovimentacaoListComponent } from './movimentacoes/movimentacao-list/movimentacao-list.component';
import { SimuladorFormComponent } from './simulador/simulador-form/simulador-form.component';
import { HomePageComponent } from './home/home-page/home-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'estados', component: EstadoListComponent },
  { path: 'estados/form', component: EstadoFormComponent },
  { path: 'estados/form/:id', component: EstadoFormComponent },
  { path: 'cidades', component: CidadeListComponent },
  { path: 'cidades/form', component: CidadeFormComponent },
  { path: 'cidades/form/:id', component: CidadeFormComponent },
  { path: 'cores', component: CorListComponent },
  { path: 'cores/form', component: CorFormComponent },
  { path: 'cores/form/:id', component: CorFormComponent },
  { path: 'marcas', component: MarcaListComponent },
  { path: 'marcas/form', component: MarcaFormComponent },
  { path: 'marcas/form/:id', component: MarcaFormComponent },
  { path: 'modelos', component: ModeloListComponent },
  { path: 'modelos/form', component: ModeloFormComponent },
  { path: 'modelos/form/:id', component: ModeloFormComponent },
  { path: 'veiculos', component: VeiculoListComponent },
  { path: 'veiculos/form', component: VeiculoFormComponent },
  { path: 'veiculos/form/:id', component: VeiculoFormComponent },
  { path: 'motoristas', component: MotoristaListComponent },
  { path: 'motoristas/form', component: MotoristaFormComponent },
  { path: 'motoristas/form/:id', component: MotoristaFormComponent },
  { path: 'viagens', component: ViagemListComponent },
  { path: 'viagens/form', component: ViagemFormComponent },
  { path: 'viagens/form/:id', component: ViagemFormComponent },
  { path: 'movimentacoes', component: MovimentacaoListComponent },
  { path: 'simulador', component: SimuladorFormComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }