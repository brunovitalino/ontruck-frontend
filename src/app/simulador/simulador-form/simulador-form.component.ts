import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Movimentacao } from 'src/app/shared/model/movimentacao';
import { MovimentacoesService } from 'src/app/movimentacoes/movimentacoes.service';
import { ViagensService } from 'src/app/viagens/viagens.service';
import { Viagem } from 'src/app/shared/model/viagem';

@Component({
  selector: 'app-simulador-form',
  templateUrl: './simulador-form.component.html',
  styleUrls: ['./simulador-form.component.css']
})
export class SimuladorFormComponent implements OnInit {

  movimentacao: Movimentacao;
  linhas: any[] = [];
  viagens: Viagem[];

  constructor(
    private movimentacoesService: MovimentacoesService,
    private router: Router,
    private viagensService: ViagensService
  ) { }

  ngOnInit() {
    this.loadViagens();
    this.loadLinhas();
  }

  loadLinhas(): void {
    this.movimentacao = { id: null, codigo: "", viagem: null, dataHora: null, velocidade: 0 };
  }

  loadViagens() {
    this.viagensService.findAll().subscribe(resp => {
      this.viagens = resp.content as Viagem[] || null;
    });
  }

  onSelectViagem(valor: number) {
    if (valor == 0) {
      this.movimentacao.viagem = null;
      return;
    }
    this.movimentacao.viagem = this.viagens.find(e => e.id == valor);
  }

  save() {
    this.movimentacoesService.save(this.movimentacao).subscribe(res => {
      this.loadLinhas();
      this.router.navigate( [`/simulador`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

}
