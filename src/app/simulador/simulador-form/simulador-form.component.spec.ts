import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimuladorFormComponent } from './simulador-form.component';

describe('SimuladorFormComponent', () => {
  let component: SimuladorFormComponent;
  let fixture: ComponentFixture<SimuladorFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimuladorFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimuladorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
