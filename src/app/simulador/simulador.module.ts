import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimuladorFormComponent } from './simulador-form/simulador-form.component';
import { CrudsModule } from '../shared/cruds/cruds.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SimuladorFormComponent
  ],
  imports: [
    CommonModule,
    CrudsModule,
    FormsModule
  ]
})
export class SimuladorModule { }
