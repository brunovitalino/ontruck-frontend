import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EstadosService } from 'src/app/estados/estados.service';
import { Cidade } from 'src/app/shared/model/cidade';
import { Estado } from 'src/app/shared/model/estado';
import { CidadesService } from '../cidades.service';

@Component({
  selector: 'app-cidade-form',
  templateUrl: './cidade-form.component.html',
  styleUrls: ['./cidade-form.component.css']
})
export class CidadeFormComponent implements OnInit, OnChanges {

  cidadeId: number;
  cidade: Cidade;
  linhas: any[] = [];
  estados: Estado[];

  constructor(
    private cidadesService: CidadesService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private estadosService: EstadosService
  ) { }

  ngOnInit(): void {
    this.loadEstados();
    this.loadCidadeId();
    this.loadLinhas();
  }

  ngOnChanges(changes: SimpleChanges): void {
    
    console.log('algo??');
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'esta': {
            this.doSomething('mudou');
          }
        }
      }
    }
  }

  doSomething(str) {
    console.log('doSome', str);
  }
  
  loadCidadeId() {
    this.cidadeId = this.activatedRoute.snapshot.params.id;
  }
  
  isNewForm() {
    return !this.cidadeId;
  }

  loadLinhas() {
    if (this.isNewForm()) {
      this.cidade = { id: null, nome: "", estado: { id: null, nome: "" } };
    } else {
      this.cidadesService.findOneById(this.cidadeId).subscribe(resp => {
        this.linhas = [];
        this.cidade = resp || null;
      });
    }
  }

  loadEstados() {
    this.estadosService.findAll().subscribe(resp => {
      this.estados = resp.content as Estado[] || null;
    });
  }

  onSelectEstado(valor: number) {
    if (valor == 0) {
      this.cidade.estado = null;
      return;
    }
    this.cidade.estado = this.estados.find(e => e.id == valor);
  }

  save() {
    this.cidadesService.save(this.cidade).subscribe(res => {
      this.router.navigate( [`/cidades`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

  update() {
    this.cidadesService.update(this.cidadeId, this.cidade).subscribe(res => {
      this.router.navigate( [`/cidades`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

}
