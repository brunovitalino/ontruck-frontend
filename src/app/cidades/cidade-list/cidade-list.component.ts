import { Component, OnInit } from '@angular/core';
import { Cidade } from 'src/app/shared/model/cidade';
import { CidadesService } from '../cidades.service';

@Component({
  selector: 'app-cidade-list',
  templateUrl: './cidade-list.component.html',
  styleUrls: ['./cidade-list.component.css']
})
export class CidadeListComponent implements OnInit {

  pageNumber: number;
  pageSize: number;
  cidades: Cidade[];
  colunas = [];
  linhas = [];

  constructor(
    private cidadesService: CidadesService
  ) { }

  ngOnInit(): void {
    this.loadPageable();
    this.loadColunas();
    this.loadLinhas();
  }

  loadPageable() {
    this.cidadesService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.cidades = resp.content as Cidade[];
    });
  }

  loadColunas() {
    this.colunas = [
      { header: 'Nome' },
      { header: 'Estado' }
    ];
  }

  loadLinhas() {
    this.cidadesService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.cidades = resp.content as Cidade[];
      this.setLinhas(this.cidades);
    });
  }

  setLinhas(cidades: Cidade[]) {
    cidades.forEach(e => {
      this.linhas.push([
        { field: e.id },
        { field: e.nome },
        { field: e.estado.nome }
      ]);        
    });
  }

  deleteLinha(id) {
    this.cidadesService.delete(id).subscribe(res => {
      this.linhas = this.linhas.filter(e => e[0]['field'] != id);
    }, err => {
      if (err.error && err.error.exception) {
        console.log('ERRO', err.error.exception, err.error.cause);
      } else {
        console.log('ERRO', err);
      }
    });
  }

}
