import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { PageResponse } from '../shared/model/page-response';
import { Cidade } from '../shared/model/cidade';
import { environment } from '../../environments/environment';

const API = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class CidadesService {

  constructor(
    private http: HttpClient
  ) { }
  
  public findAll() {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json'
    });
    return this.http.get<PageResponse>(`${API}/cidades`, {headers});
  }
  
  public findOneById(id: number) {
    return this.http.get<Cidade>(`${API}/cidades/${id}`);
  }
  
  public save(cidade: Cidade) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.post<Cidade>(`${API}/cidades`, cidade, {headers});
  }
  
  public update(id: number, cidade: Cidade) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.put<Cidade>(`${API}/cidades/${id}`, cidade, {headers});
  }
  
  public delete(id: number) {
    return this.http.delete<Cidade>(`${API}/cidades/${id}`);
  }

}
