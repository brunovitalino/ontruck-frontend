import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CidadeListComponent } from './cidade-list/cidade-list.component';
import { CidadeFormComponent } from './cidade-form/cidade-form.component';
import { CrudsModule } from '../shared/cruds/cruds.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CidadeListComponent,
    CidadeFormComponent
  ],
  imports: [
    CommonModule,
    CrudsModule,
    FormsModule
  ]
})
export class CidadesModule { }
