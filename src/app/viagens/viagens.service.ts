import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { PageResponse } from '../shared/model/page-response';
import { Viagem } from '../shared/model/viagem';
import { environment } from '../../environments/environment';

const API = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ViagensService {

  constructor(
    private http: HttpClient
  ) { }
  
  public findAll() {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json'
    });
    return this.http.get<PageResponse>(`${API}/viagens`, {headers});
  }
  
  public findOneById(id: number) {
    return this.http.get<Viagem>(`${API}/viagens/${id}`);
  }
  
  public save(viagem: Viagem) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.post<Viagem>(`${API}/viagens`, viagem, {headers});
  }
  
  public update(id: number, viagem: Viagem) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.put<Viagem>(`${API}/viagens/${id}`, viagem, {headers});
  }
  
  public delete(id: number) {
    return this.http.delete<Viagem>(`${API}/viagens/${id}`);
  }

}
