import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViagemListComponent } from './viagem-list/viagem-list.component';
import { ViagemFormComponent } from './viagem-form/viagem-form.component';
import { CrudsModule } from '../shared/cruds/cruds.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ViagemListComponent,
    ViagemFormComponent
  ],
  imports: [
    CommonModule,
    CrudsModule,
    FormsModule
  ]
})
export class ViagensModule { }
