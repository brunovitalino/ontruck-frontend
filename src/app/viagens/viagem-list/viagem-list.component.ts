import { DatePipe, registerLocaleData } from '@angular/common';
import { Component, LOCALE_ID, OnInit } from '@angular/core';

import { Viagem } from 'src/app/shared/model/viagem';
import { ViagensService } from '../viagens.service';

@Component({
  selector: 'app-viagem-list',
  templateUrl: './viagem-list.component.html',
  styleUrls: ['./viagem-list.component.css']
})
export class ViagemListComponent implements OnInit {
  

  pageNumber: number;
  pageSize: number;
  viagens: Viagem[];
  colunas = [];
  linhas = [];
  datePipe = new DatePipe('pt-BR');

  constructor(
    private viagensService: ViagensService
  ) { }

  ngOnInit(): void {
    this.loadPageable();
    this.loadColunas();
    this.loadLinhas();
  }

  loadPageable() {
    this.viagensService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.viagens = resp.content as Viagem[];
    });
  }

  loadColunas() {
    this.colunas = [
      { header: 'ID Viagem' },
      { header: 'Data' },
      { header: 'Origem' },
      { header: 'Destino' },
      { header: 'Veículo' },
      { header: 'Motorista 1' },
      { header: 'Motorista 2' }
    ];
  }

  loadLinhas() {
    this.viagensService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.viagens = resp.content as Viagem[];
      this.setLinhas(this.viagens);
    });
  }

  setLinhas(viagens: Viagem[]) {
    viagens.forEach(e => {
      this.linhas.push([
        { field: e.id },
        { field: e.id },
        { field: this.datePipe.transform(e.data, 'dd/MM/yyyy') },
        { field: e.origem.nome },
        { field: e.destino.nome },
        { field: e.veiculo.placa },
        { field: e.motorista1.nome },
        { field: e.motorista2.nome }
      ]);        
    });
  }

  deleteLinha(id) {
    this.viagensService.delete(id).subscribe(res => {
      this.linhas = this.linhas.filter(e => e[0]['field'] != id);
    }, err => {
      if (err.error && err.error.exception) {
        console.log('ERRO', err.error.exception, err.error.cause);
      } else {
        console.log('ERRO', err);
      }
    });
  }

}
