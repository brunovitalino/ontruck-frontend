import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Viagem } from 'src/app/shared/model/viagem';
import { ViagensService } from '../viagens.service';
import { Cidade } from 'src/app/shared/model/cidade';
import { CidadesService } from 'src/app/cidades/cidades.service';
import { Motorista } from 'src/app/shared/model/motorista';
import { MotoristasService } from 'src/app/motoristas/motoristas.service';
import { Veiculo } from 'src/app/shared/model/veiculo';
import { VeiculosService } from 'src/app/veiculos/veiculos.service';

@Component({
  selector: 'app-viagem-form',
  templateUrl: './viagem-form.component.html',
  styleUrls: ['./viagem-form.component.css']
})
export class ViagemFormComponent implements OnInit {

  viagemId: number;
  viagem: Viagem;
  linhas: any[] = [];
  cidades: Cidade[];
  veiculos: Veiculo[];
  motoristas: Motorista[];

  constructor(
    private viagensService: ViagensService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private cidadesService: CidadesService,
    private veiculosService: VeiculosService,
    private motoristasService: MotoristasService
  ) { }

  ngOnInit(): void {
    this.loadCidades();
    this.loadVeiculos();
    this.loadMotoristas();
    this.loadViagemId();
    this.loadLinhas();
  }
  
  loadViagemId() {
    this.viagemId = this.activatedRoute.snapshot.params.id;
  }
  
  isNewForm() {
    return !this.viagemId;
  }

  loadLinhas() {
    if (this.isNewForm()) {
      this.viagem = { id: null, data: new Date(),
        origem: { id: null, nome: "", estado: {id: null, nome: ""} },
        destino: { id: null, nome: "", estado: {id: null, nome: ""} },
        veiculo: { id: null, placa: "", cor: null, modelo: null, ano: null, kmTotal: null },
        motorista1: { id: null, cpf: "", nome: "" },
        motorista2: { id: null, cpf: "", nome: "" }
      }
    } else {
      this.viagensService.findOneById(this.viagemId).subscribe(resp => {
        this.linhas = [];
        this.viagem = resp || null;
      });
    }
  }

  loadCidades() {
    this.cidadesService.findAll().subscribe(resp => {
      this.cidades = resp.content as Cidade[] || null;
    });
  }

  onSelectOrigem(valor: number) {
    if (valor == 0) {
      this.viagem.origem = null;
      return;
    }
    this.viagem.origem = this.cidades.find(e => e.id == valor);
  }

  onSelectDestino(valor: number) {
    if (valor == 0) {
      this.viagem.destino = null;
      return;
    }
    this.viagem.destino = this.cidades.find(e => e.id == valor);
  }

  loadVeiculos() {
    this.veiculosService.findAll().subscribe(resp => {
      this.veiculos = resp.content as Veiculo[] || null;
    });
  }

  onSelectVeiculo(valor: number) {
    if (valor == 0) {
      this.viagem.veiculo = null;
      return;
    }
    this.viagem.veiculo = this.veiculos.find(e => e.id == valor);
  }

  loadMotoristas() {
    this.motoristasService.findAll().subscribe(resp => {
      this.motoristas = resp.content as Motorista[] || null;
    });
  }

  onSelectMotorista1(valor: number) {
    if (valor == 0) {
      this.viagem.motorista1 = null;
      return;
    }
    this.viagem.motorista1 = this.motoristas.find(e => e.id == valor);
  }

  onSelectMotorista2(valor: number) {
    if (valor == 0) {
      this.viagem.motorista2 = null;
      return;
    }
    this.viagem.motorista2 = this.motoristas.find(e => e.id == valor);
  }

  save() {
    this.viagensService.save(this.viagem).subscribe(res => {
      this.router.navigate( [`/viagens`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

  update() {
    this.viagensService.update(this.viagemId, this.viagem).subscribe(res => {
      this.router.navigate( [`/viagens`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

}
