import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViagemFormComponent } from './viagem-form.component';

describe('ViagemFormComponent', () => {
  let component: ViagemFormComponent;
  let fixture: ComponentFixture<ViagemFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViagemFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViagemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
