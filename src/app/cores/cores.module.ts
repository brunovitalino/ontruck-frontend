import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CorListComponent } from './cor-list/cor-list.component';
import { CorFormComponent } from './cor-form/cor-form.component';
import { CrudsModule } from '../shared/cruds/cruds.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CorListComponent,
    CorFormComponent
  ],
  imports: [
    CommonModule,
    CrudsModule,
    FormsModule
  ]
})
export class CoresModule { }
