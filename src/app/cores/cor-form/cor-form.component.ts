import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Cor } from 'src/app/shared/model/cor';
import { CoresService } from '../cores.service';


@Component({
  selector: 'app-cor-form',
  templateUrl: './cor-form.component.html',
  styleUrls: ['./cor-form.component.css']
})
export class CorFormComponent implements OnInit {

  corId: number;
  cor: Cor;
  linhas: any[] = [];

  constructor(
    private coresService: CoresService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadCorId();
    this.loadLinhas();
  }
  
  loadCorId(): void {
    this.corId = this.activatedRoute.snapshot.params.id;
  }
  
  isNewForm(): boolean {
    return !this.corId;
  }

  loadLinhas(): void {
    if (this.isNewForm()) {
      this.cor = { id: null, nome: "" };
      this.setLinhas(this.cor);
    } else {
      this.coresService.findOneById(this.corId).subscribe(resp => {
        this.linhas = [];
        this.cor = resp || null;
        this.setLinhas(this.cor);
      });
    }
  }

  setLinhas(cor: Cor): void {
    if (!cor) {
      return;
    }
    const keys = Object.keys(cor);
    for (let i = 1; i < keys.length; i+=2) {
      this.linhas.push(keys.slice(i, i+2));
    }
  }

  save() {
    this.coresService.save(this.cor).subscribe(res => {
      this.router.navigate( [`/cores`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

  update() {
    this.coresService.update(this.corId, this.cor).subscribe(res => {
      this.router.navigate( [`/cores`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

}
