import { Component, OnInit } from '@angular/core';
import { Cor } from 'src/app/shared/model/cor';
import { CoresService } from '../cores.service';

@Component({
  selector: 'app-cor-list',
  templateUrl: './cor-list.component.html',
  styleUrls: ['./cor-list.component.css']
})
export class CorListComponent implements OnInit {

  pageNumber: number;
  pageSize: number;
  cores: Cor[];
  colunas = [];
  linhas = [];

  constructor(
    private coresService: CoresService
  ) { }

  ngOnInit(): void {
    this.loadPageable();
    this.loadColunas();
    this.loadLinhas();
  }

  loadPageable() {
    this.coresService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.cores = resp.content as Cor[];
    });
  }

  loadColunas() {
    this.colunas = [
      { header: 'Nome' }
    ];
  }

  loadLinhas() {
    this.coresService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.cores = resp.content as Cor[];
      this.setLinhas(this.cores);
    });
  }

  setLinhas(cores: Cor[]) {
    cores.forEach(e => {
      this.linhas.push([
        { field: e.id },
        { field: e.nome }
      ]);        
    });
  }

  deleteLinha(id) {
    this.coresService.delete(id).subscribe(res => {
      this.linhas = this.linhas.filter(e => e[0]['field'] != id);
    }, err => {
      if (err.error && err.error.exception) {
        console.log('ERRO', err.error.exception, err.error.cause);
      } else {
        console.log('ERRO', err);
      }
    });
  }


}
