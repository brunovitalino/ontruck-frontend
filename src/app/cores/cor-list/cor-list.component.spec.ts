import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorListComponent } from './cor-list.component';

describe('CorListComponent', () => {
  let component: CorListComponent;
  let fixture: ComponentFixture<CorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
