import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { PageResponse } from '../shared/model/page-response';
import { Cor } from '../shared/model/cor';
import { environment } from '../../environments/environment';

const API = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class CoresService {

  constructor(
    private http: HttpClient
  ) { }
  
  public findAll() {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json'
    });
    return this.http.get<PageResponse>(`${API}/cores`, {headers});
  }
  
  public findOneById(id: number) {
    return this.http.get<Cor>(`${API}/cores/${id}`);
  }
  
  public save(cor: Cor) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.post<Cor>(`${API}/cores`, cor, {headers});
  }
  
  public update(id: number, cor: Cor) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.put<Cor>(`${API}/cores/${id}`, cor, {headers});
  }
  
  public delete(id: number) {
    return this.http.delete<Cor>(`${API}/cores/${id}`);
  }

}
