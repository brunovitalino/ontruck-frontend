import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { faEdit, faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-crud-list',
  templateUrl: './crud-list.component.html',
  styleUrls: ['./crud-list.component.css']
})
export class CrudListComponent implements OnInit {

  @Input() titulo = '';
  @Input() colunas = [];
  @Input() linhas = [];
  @Output() removeEvent = new EventEmitter<number>();
  @Input() showIconRemove = true;
  @Input() showIconEdit = true;
  @Input() showIconAdd = true;

  iconAdd = faPencilAlt;
  iconEdit = faEdit;
  iconRemove = faTrash;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  add(): void {
    this.router.navigate( [this.titulo.toLowerCase() + `/form`] );
  }

  edit(id: number): void {
    this.router.navigate( [this.titulo.toLowerCase() + `/form/${id}`] );
  }

  remove(id: number): void {
    this.removeEvent.emit(id);
  }

}
