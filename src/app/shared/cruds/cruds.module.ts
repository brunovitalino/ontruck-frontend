import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CrudListComponent } from './crud-list/crud-list.component';

@NgModule({
  declarations: [CrudListComponent],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [CrudListComponent]
})
export class CrudsModule { }
