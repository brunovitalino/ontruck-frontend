
export interface Motorista {
  id: number,
  cpf: string,
  nome: string
}
