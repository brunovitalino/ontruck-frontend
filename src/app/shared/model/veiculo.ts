import { Cor } from "./cor";
import { Modelo } from "./modelo";

export interface Veiculo {
  id: number,
  placa: string,
  cor: Cor,
  modelo: Modelo,
  ano: number,
  kmTotal: number
}
