import { Viagem } from "./viagem";

export interface Movimentacao {
	id: number, // <= não é usado
  codigo: string,
	viagem: Viagem,
	dataHora: Date,
	velocidade: number
}
