import { Cidade } from "./cidade";
import { Motorista } from "./motorista";
import { Veiculo } from "./veiculo";

export interface Viagem {
  id: number,
	data: Date;
	motorista1: Motorista;
	motorista2?: Motorista;
	veiculo: Veiculo;
	origem: Cidade;
	destino: Cidade;
}
