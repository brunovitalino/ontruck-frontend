import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { PageResponse } from '../shared/model/page-response';
import { Modelo } from '../shared/model/modelo';
import { environment } from '../../environments/environment';

const API = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ModelosService {

  constructor(
    private http: HttpClient
  ) { }
  
  public findAll() {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json'
    });
    return this.http.get<PageResponse>(`${API}/modelos`, {headers});
  }
  
  public findOneById(id: number) {
    return this.http.get<Modelo>(`${API}/modelos/${id}`);
  }
  
  public save(modelo: Modelo) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.post<Modelo>(`${API}/modelos`, modelo, {headers});
  }
  
  public update(id: number, modelo: Modelo) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.put<Modelo>(`${API}/modelos/${id}`, modelo, {headers});
  }
  
  public delete(id: number) {
    return this.http.delete<Modelo>(`${API}/modelos/${id}`);
  }

}
