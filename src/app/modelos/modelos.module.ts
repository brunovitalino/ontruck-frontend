import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModeloListComponent } from './modelo-list/modelo-list.component';
import { ModeloFormComponent } from './modelo-form/modelo-form.component';
import { CrudsModule } from '../shared/cruds/cruds.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ModeloListComponent,
    ModeloFormComponent
  ],
  imports: [
    CommonModule,
    CrudsModule,
    FormsModule
  ]
})
export class ModelosModule { }
