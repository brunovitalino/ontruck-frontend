import { Component, OnInit } from '@angular/core';
import { Modelo } from 'src/app/shared/model/modelo';
import { ModelosService } from '../modelos.service';

@Component({
  selector: 'app-modelo-list',
  templateUrl: './modelo-list.component.html',
  styleUrls: ['./modelo-list.component.css']
})
export class ModeloListComponent implements OnInit {

  pageNumber: number;
  pageSize: number;
  modelos: Modelo[];
  colunas = [];
  linhas = [];

  constructor(
    private modelosService: ModelosService
  ) { }

  ngOnInit(): void {
    this.loadPageable();
    this.loadColunas();
    this.loadLinhas();
  }

  loadPageable() {
    this.modelosService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.modelos = resp.content as Modelo[];
    });
  }

  loadColunas() {
    this.colunas = [
      { header: 'Nome' },
      { header: 'Marca' }
    ];
  }

  loadLinhas() {
    this.modelosService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.modelos = resp.content as Modelo[];
      this.setLinhas(this.modelos);
    });
  }

  setLinhas(modelos: Modelo[]) {
    modelos.forEach(e => {
      this.linhas.push([
        { field: e.id },
        { field: e.nome },
        { field: e.marca.nome }
      ]);        
    });
  }

  deleteLinha(id) {
    this.modelosService.delete(id).subscribe(res => {
      this.linhas = this.linhas.filter(e => e[0]['field'] != id);
    }, err => {
      if (err.error && err.error.exception) {
        console.log('ERRO', err.error.exception, err.error.cause);
      } else {
        console.log('ERRO', err);
      }
    });
  }

}
