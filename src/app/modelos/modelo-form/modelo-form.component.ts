import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { MarcasService } from 'src/app/marcas/marcas.service';
import { Marca } from 'src/app/shared/model/marca';
import { Modelo } from 'src/app/shared/model/modelo';
import { ModelosService } from '../modelos.service';

@Component({
  selector: 'app-modelo-form',
  templateUrl: './modelo-form.component.html',
  styleUrls: ['./modelo-form.component.css']
})
export class ModeloFormComponent implements OnInit {

  modeloId: number;
  modelo: Modelo;
  linhas: any[] = [];
  marcas: Marca[];

  constructor(
    private modelosService: ModelosService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private marcasService: MarcasService
  ) { }

  ngOnInit(): void {
    this.loadMarcas();
    this.loadModeloId();
    this.loadLinhas();
  }
  
  loadModeloId() {
    this.modeloId = this.activatedRoute.snapshot.params.id;
  }
  
  isNewForm() {
    return !this.modeloId;
  }

  loadLinhas() {
    if (this.isNewForm()) {
      this.modelo = { id: null, nome: "", marca: { id: null, nome: "" } };
    } else {
      this.modelosService.findOneById(this.modeloId).subscribe(resp => {
        this.linhas = [];
        this.modelo = resp || null;
      });
    }
  }

  loadMarcas() {
    this.marcasService.findAll().subscribe(resp => {
      this.marcas = resp.content as Marca[] || null;
    });
  }

  onSelectMarca(valor: number) {
    if (valor == 0) {
      this.modelo.marca = null;
      return;
    }
    this.modelo.marca = this.marcas.find(e => e.id == valor);
  }

  save() {
    this.modelosService.save(this.modelo).subscribe(res => {
      this.router.navigate( [`/modelos`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

  update() {
    this.modelosService.update(this.modeloId, this.modelo).subscribe(res => {
      this.router.navigate( [`/modelos`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

}
