import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovimentacaoListComponent } from './movimentacao-list/movimentacao-list.component';
import { CrudsModule } from '../shared/cruds/cruds.module';

@NgModule({
  declarations: [
    MovimentacaoListComponent
  ],
  imports: [
    CommonModule,
    CrudsModule
  ]
})
export class MovimentacoesModule { }
