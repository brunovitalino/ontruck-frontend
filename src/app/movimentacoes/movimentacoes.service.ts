import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { PageResponse } from '../shared/model/page-response';
import { Movimentacao } from '../shared/model/movimentacao';
import { environment } from 'src/environments/environment';

const API = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class MovimentacoesService {

  constructor(
    private http: HttpClient
  ) { }
  
  public findAll() {
    const headers = new HttpHeaders({
      'Accept':'application/json'
    });
    return this.http.get<PageResponse>(`${API}/movimentacoes`, {headers});
  }
  
  public findAllByViagemId(id: number) {
    return this.http.get<Movimentacao>(`${API}/movimentacoes/viagem/${id}`);
  }
  
  public save(movimentacao: Movimentacao) {
    const headers = new HttpHeaders({
      'Accept':'application/json',
      'Content-Type':'application/json'
    });
    return this.http.post<Movimentacao>(`${API}/movimentacoes`, movimentacao, {headers});
  }
  
  public delete(id: number) {
    return this.http.delete<Movimentacao>(`${API}/movimentacoes/${id}`);
  }

}
