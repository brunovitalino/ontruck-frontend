import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Movimentacao } from 'src/app/shared/model/movimentacao';
import { MovimentacoesService } from '../movimentacoes.service';

@Component({
  selector: 'app-movimentacao-list',
  templateUrl: './movimentacao-list.component.html',
  styleUrls: ['./movimentacao-list.component.css']
})
export class MovimentacaoListComponent implements OnInit {

  pageNumber: number;
  pageSize: number;
  movimentacoes: Movimentacao[];
  colunas = [];
  linhas = [];
  datePipe = new DatePipe('pt-BR');

  constructor(
    private movimentacoesService: MovimentacoesService
  ) { }

  ngOnInit(): void {
    this.loadColunas();
    this.loadLinhas();
  }

  loadColunas() {
    this.colunas = [
      { header: 'Id de Viagem' },
      { header: 'Data da Viagem' },
      { header: 'Data de Movimentação' },
      { header: 'Horario de Movimentação' },
      { header: 'Velocidade' }
    ];
  }

  loadLinhas() {
    this.movimentacoesService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.movimentacoes = resp.content as Movimentacao[];
      this.setLinhas(this.movimentacoes);
    });
  }

  setLinhas(movimentacoes: Movimentacao[]) {
    movimentacoes.forEach(m => {
      this.linhas.push([
        { field: m.id }, // <= não é usado
        { field: m.viagem.id ? m.viagem.id : '-' },
        { field: m.viagem.data ? this.datePipe.transform(m.viagem.data, 'dd/MM/yyyy') : '-' },
        { field: this.datePipe.transform(m.dataHora, 'dd/MM/yyyy') },
        { field: this.datePipe.transform(m.dataHora, 'hh:mm:ss') },
        { field: m.velocidade }
      ]);        
    });
  }

  deleteLinha(id) {
    this.movimentacoesService.delete(id).subscribe(res => {
      this.linhas = this.linhas.filter(e => e[0]['field'] != id);
    }, err => {
      if (err.error && err.error.exception) {
        console.log('ERRO', err.error.exception, err.error.cause);
      } else {
        console.log('ERRO', err);
      }
    });
  }

}
