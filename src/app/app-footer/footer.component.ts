import { Component, OnInit } from '@angular/core';
import { faHome, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
//import { faHome, faPlusCircle, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
//import { FaStackItemSizeDirective } from '@fortawesome/angular-fontawesome';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  public iconFaHome = faHome;
  public iconFaPlusCircle = faPlusCircle;

  constructor() { }

  ngOnInit(): void {
  }

}
