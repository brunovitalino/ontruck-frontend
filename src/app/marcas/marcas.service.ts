import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { PageResponse } from '../shared/model/page-response';
import { Marca } from '../shared/model/marca';
import { environment } from '../../environments/environment';

const API = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class MarcasService {

  constructor(
    private http: HttpClient
  ) { }
  
  public findAll() {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json'
    });
    return this.http.get<PageResponse>(`${API}/marcas`, {headers});
  }
  
  public findOneById(id: number) {
    return this.http.get<Marca>(`${API}/marcas/${id}`);
  }
  
  public save(marca: Marca) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.post<Marca>(`${API}/marcas`, marca, {headers});
  }
  
  public update(id: number, marca: Marca) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.put<Marca>(`${API}/marcas/${id}`, marca, {headers});
  }
  
  public delete(id: number) {
    return this.http.delete<Marca>(`${API}/marcas/${id}`);
  }
}
