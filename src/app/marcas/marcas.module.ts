import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarcaFormComponent } from './marca-form/marca-form.component';
import { MarcaListComponent } from './marca-list/marca-list.component';
import { CrudsModule } from '../shared/cruds/cruds.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    MarcaListComponent,
    MarcaFormComponent
  ],
  imports: [
    CommonModule,
    CrudsModule,
    FormsModule
  ]
})
export class MarcasModule { }
