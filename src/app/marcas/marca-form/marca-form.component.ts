import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Marca } from 'src/app/shared/model/marca';
import { MarcasService } from '../marcas.service';

@Component({
  selector: 'app-marca-form',
  templateUrl: './marca-form.component.html',
  styleUrls: ['./marca-form.component.css']
})
export class MarcaFormComponent implements OnInit {

  marcaId: number;
  marca: Marca;
  linhas: any[] = [];
  teste: string;

  constructor(
    private marcasService: MarcasService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadMarcaId();
    this.loadLinhas();
  }
  
  loadMarcaId(): void {
    this.marcaId = this.activatedRoute.snapshot.params.id;
  }
  
  isNewForm(): boolean {
    return !this.marcaId;
  }

  loadLinhas(): void {
    if (this.isNewForm()) {
      this.marca = { id: null, nome: "" };
      this.setLinhas(this.marca);
    } else {
      this.marcasService.findOneById(this.marcaId).subscribe(resp => {
        this.linhas = [];
        this.marca = resp || null;
        this.setLinhas(this.marca);
      });
    }
  }

  setLinhas(marca: Marca): void {
    if (!marca) {
      return;
    }
    const keys = Object.keys(marca);
    for (let i = 1; i < keys.length; i+=2) {
      this.linhas.push(keys.slice(i, i+2));
    }
  }

  save() {
    this.marcasService.save(this.marca).subscribe(res => {
      this.router.navigate( [`/marcas`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

  update() {
    this.marcasService.update(this.marcaId, this.marca).subscribe(res => {
      this.router.navigate( [`/marcas`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

}
