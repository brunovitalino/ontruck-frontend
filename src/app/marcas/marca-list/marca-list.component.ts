import { Component, OnInit } from '@angular/core';

import { Marca } from 'src/app/shared/model/marca';
import { MarcasService } from '../marcas.service';

@Component({
  selector: 'app-marca-list',
  templateUrl: './marca-list.component.html',
  styleUrls: ['./marca-list.component.css']
})
export class MarcaListComponent implements OnInit {

  pageNumber: number;
  pageSize: number;
  marcas: Marca[];
  colunas = [];
  linhas = [];

  constructor(
    private marcasService: MarcasService
  ) { }

  ngOnInit(): void {
    this.loadPageable();
    this.loadColunas();
    this.loadLinhas();
  }

  loadPageable() {
    this.marcasService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.marcas = resp.content as Marca[];
    });
  }

  loadColunas() {
    this.colunas = [
      { header: 'Nome' }
    ];
  }

  loadLinhas() {
    this.marcasService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.marcas = resp.content as Marca[];
      this.setLinhas(this.marcas);
    });
  }

  setLinhas(marcas: Marca[]) {
    marcas.forEach(e => {
      this.linhas.push([
        { field: e.id },
        { field: e.nome }
      ]);        
    });
  }

  deleteLinha(id) {
    this.marcasService.delete(id).subscribe(res => {
      this.linhas = this.linhas.filter(e => e[0]['field'] != id);
    }, err => {
      if (err.error && err.error.exception) {
        console.log('ERRO', err.error.exception, err.error.cause);
      } else {
        console.log('ERRO', err);
      }
    });
  }

}
