import { Component, OnInit } from '@angular/core';

import { Motorista } from 'src/app/shared/model/motorista';
import { MotoristasService } from '../motoristas.service';

@Component({
  selector: 'app-motorista-list',
  templateUrl: './motorista-list.component.html',
  styleUrls: ['./motorista-list.component.css']
})
export class MotoristaListComponent implements OnInit {

  pageNumber: number;
  pageSize: number;
  motoristas: Motorista[];
  colunas = [];
  linhas = [];

  constructor(
    private motoristasService: MotoristasService
  ) { }

  ngOnInit(): void {
    this.loadPageable();
    this.loadColunas();
    this.loadLinhas();
  }

  loadPageable(): void {
    this.motoristasService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.motoristas = resp.content as Motorista[];
    });
  }

  loadColunas(): void {
    this.colunas = [
      { header: 'CPF' },
      { header: 'Nome' }
    ];
  }

  loadLinhas(): void {
    this.motoristasService.findAll().subscribe(resp => {
      this.pageNumber = resp.number;
      this.pageSize = resp.size;
      this.motoristas = resp.content as Motorista[];
      this.setLinhas(this.motoristas);
    });
  }

  setLinhas(motoristas: Motorista[]): void {
    motoristas.forEach(e => {
      this.linhas.push([
        { field: e.id },
        { field: e.cpf },
        { field: e.nome }
      ]);        
    });
  }

  deleteLinha(id): void {
    this.motoristasService.delete(id).subscribe(res => {
      this.linhas = this.linhas.filter(e => e[0]['field'] != id);
    }, err => {
      if (err.error && err.error.exception) {
        console.log('ERRO', err.error.exception, err.error.cause);
      } else {
        console.log('ERRO', err);
      }
    });
  }

}
