import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MotoristaListComponent } from './motorista-list/motorista-list.component';
import { MotoristaFormComponent } from './motorista-form/motorista-form.component';
import { CrudsModule } from '../shared/cruds/cruds.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    MotoristaListComponent,
    MotoristaFormComponent
  ],
  imports: [
    CommonModule,
    CrudsModule,
    FormsModule
  ]
})
export class MotoristasModule { }
