import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Motorista } from 'src/app/shared/model/motorista';
import { MotoristasService } from '../motoristas.service';


@Component({
  selector: 'app-motorista-form',
  templateUrl: './motorista-form.component.html',
  styleUrls: ['./motorista-form.component.css']
})
export class MotoristaFormComponent implements OnInit {

  motoristaId: number;
  motorista: Motorista;
  linhas: any[] = [];
  teste: string;

  constructor(
    private motoristasService: MotoristasService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadMotoristaId();
    this.loadLinhas();
  }
  
  loadMotoristaId(): void {
    this.motoristaId = this.activatedRoute.snapshot.params.id;
  }
  
  isNewForm(): boolean {
    return !this.motoristaId;
  }

  loadLinhas(): void {
    if (this.isNewForm()) {
      this.motorista = { id: null, cpf: "", nome: "" };
      this.setLinhas(this.motorista);
    } else {
      this.motoristasService.findOneById(this.motoristaId).subscribe(resp => {
        this.linhas = [];
        this.motorista = resp || null;
        this.setLinhas(this.motorista);
      });
    }
  }

  setLinhas(motorista: Motorista): void {
    if (!motorista) {
      return;
    }
    const keys = Object.keys(motorista);
    for (let i = 1; i < keys.length; i+=2) {
      this.linhas.push(keys.slice(i, i+2));
    }
  }

  save() {
    this.motoristasService.save(this.motorista).subscribe(res => {
      this.router.navigate( [`/motoristas`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

  update() {
    this.motoristasService.update(this.motoristaId, this.motorista).subscribe(res => {
      this.router.navigate( [`/motoristas`] );
    }, err => {
      console.log('ERRO', err);
    });
  }

}
