import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Motorista } from '../shared/model/motorista';
import { PageResponse } from '../shared/model/page-response';
import { environment } from '../../environments/environment';

const API = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class MotoristasService {

  constructor(
    private http: HttpClient
  ) { }
  
  public findAll() {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json'
    });
    return this.http.get<PageResponse>(`${API}/motoristas`, {headers});
  }
  
  public findOneById(id: number) {
    return this.http.get<Motorista>(`${API}/motoristas/${id}`);
  }
  
  public save(motorista: Motorista) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json'
    });
    return this.http.post<Motorista>(`${API}/motoristas`, motorista, {headers});
  }
  
  public update(id: number, motorista: Motorista) {
    const headers = new HttpHeaders({
      // 'Origin':'*'
      // 'Access-Control-Allow-Origin':'*'
      'Accept':'application/json',
      // // 'Access-Control-Request-Headers':'Content-Type',
      'Content-Type':'application/json'
    });
    return this.http.put<Motorista>(`${API}/motoristas/${id}`, motorista, {headers});
  }
  
  public delete(id: number) {
    return this.http.delete<Motorista>(`${API}/motoristas/${id}`);
  }

}
